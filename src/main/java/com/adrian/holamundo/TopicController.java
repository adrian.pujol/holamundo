package com.adrian.holamundo;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class TopicController {

    @RequestMapping(value = "/Topics", method = RequestMethod.GET)
    public List<Topic> ObtenerTemitas(){
        return Arrays.asList(
                new Topic("Spring","Spring Framework","Spring Framework Description"),
                new Topic("Java","Core Java","Core Java Description"),
                new Topic("JavaScript","JavaScript","JavaScript Description"));
    }

}
